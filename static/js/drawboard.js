const PIECES = 60;
const CENTERID = (PIECES*PIECES*2)+PIECES;
const shadow = "-moz-box-shadow: -13px 14px 17px #435; -webkit-box-shadow: -13px 14px 17px #435; box-shadow: -13px 14px 17px #435;"
let lastDrawn;
let lastMeeple;
let meeplePhase = false;

function drawBoard() {
    const table = document.createElement("table");
    for (var row = 0; row <= PIECES*2; row++) {
        const tr = document.createElement("tr");
        for (var column = 0; column <= PIECES*2; column++) {
            const td = document.createElement("td");
            td.id = column + row*PIECES*2;
            td.className = 'boardtile'
            tr.appendChild(td);
        }
        table.appendChild(tr);
    };
    document.body.appendChild(table)

    firstTileSetup()

}

function firstTileSetup() {
    const c = get(CENTERID)
    makeNeighborsDrawable(CENTERID)
    c.id = "centerTile"
    c.scrollIntoView({behavior: "smooth", block: "center", inline: "center"})
}

function drawable(event) {
    if (meeplePhase) { return; }
    socket.emit('put tile on', event.srcElement.id)
}

function drawOn(tileId) {
    if (!tileId || meeplePhase) { return; }
    eraseLast()
    drawTile(tileId)
    lastDrawn = tileId;
}

function eraseLast() {
    if (lastDrawn) {
        const last = get(lastDrawn)
        last.style.backgroundImage = null;
    }
}

function afterLegality(center) {
    const tile = get(center)
    tile.removeEventListener("click", drawable)
    tile.clicked = true
    tile.listener = false

    const neighbors = [1, -1, PIECES*2, -PIECES*2]
    neighbors.forEach((n) => {
        const nid = n + Number(tile.id)
        if(nid != CENTERID) {
            const t = document.getElementById(nid)
            if (!t.clicked && !t.listener) {
                t.addEventListener("click", drawable)
                t.listener = true
            }
        }
    })
}

function makeNeighborsDrawable(centerId){
    const neighbors = [1, -1, PIECES*2, -PIECES*2]
    neighbors.forEach((n) => {
        const nid = n + Number(centerId)
        if(nid != CENTERID) {
            const t = document.getElementById(nid)
            if (!t.clicked && !t.listener) {
                t.addEventListener("click", drawable)
                t.listener = true
            }
        }
    })
}

window.onload = drawBoard;
