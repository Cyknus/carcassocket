const socket = io();
const IAm = window.prompt("tu nombre:", "fulanito");
let myId;

socket.emit('new player', IAm);
socket.on('player in', updateRoster);
socket.on('playing tile', showCurrentTile);
socket.on('draw tile', drawOn);
socket.on('rotate to', rotateTo);
socket.on('put a meeple', startMeeplePhase);
socket.on('draw meeple', drawMeepleOn);
socket.on('remove last meeple', removeMeeple)
socket.on('next player', nextTurn)

function updateRoster(state) {
    const roster = get('roster');
    removeChildren(roster)
    state.players.forEach((p, idx) => {
        const name = document.createElement('p');
        name.innerHTML = p.name
        myId = myId || (p.name == IAm ? p.id : undefined);
        if (state.turn == idx) {
            name.style.fontWeight = 'bold';
        }
        roster.appendChild(name)
    })
}

function beginMatch() {
    get('main_bttn').disabled = true;
    get('main_bttn').value = 'Esperando\na los demas'
    socket.emit('begin match');
}

function showCurrentTile(state) {
    changeMainButt('juega', 'submitPlay(lastDrawn)', state)
    const playingTile = get('playingTile');
    playingTile.style.display = 'inline-flex'
    const tile = get('playingTileImg');
    tile.src = `static/img/${state.currentTile}.png`;
}

function removeMeeple(id) {
    removeChildren(get(id))
}

function removeChildren(elem) {
    for (var i = elem.children.length - 1; i >= 0; i--) {
        elem.removeChild(elem.children[i])
    }
}

function drawTile(tileId) {
    const onto = get(tileId)
    const like = get('playingTileImg')
    onto.style.position = 'relative'
    onto.style.backgroundImage = `url(${like.src})`
    onto.style.backgroundRepeat = 'no-repeat'
    onto.style.backgroundPosition = 'center center'
    onto.style.transform = like.style.transform
}

function rotate(direction) {
    const tile = get("playingTileImg");
    orientation = tile.orientation || 0;
    orientation = (Number(orientation)+90*direction) % 360
    socket.emit('rotate tile', orientation);
}

function rotateTo(orientation) {
    const tile = get("playingTileImg");
    tile.style.transform = 'rotate(' + orientation + 'deg)'
    tile.orientation = orientation
    if (lastDrawn) {
        last = get(lastDrawn)
        last.style.transform = 'rotate(' + orientation + 'deg)'
    }
}

function nextTurn(state) {
    const lastM = get(lastMeeple);
    lastM.id = null;
    meeplePhase = false;
    removeMeepleListeners()
    lastDrawn = null
    showCurrentTile(state);
}

function startMeeplePhase(state) {
    meeplePhase = true;
    afterLegality(lastDrawn);
    // check if player has enough meeples
    changeMainButt("Pon Meeple", "submitMeeple()", state);

    const workingTile = get(lastDrawn);

    const baseGrid = createBaseGrid()
    baseGrid.innerHTML = state.gridHtml;
    addMeepleListeners(baseGrid)

    workingTile.appendChild(baseGrid)
}

function changeMainButt(value, fnStr, state) {
    const currentPlayer = state.players[state.turn]
    const mainb = get('main_bttn');
    if (currentPlayer.id == myId) {
        mainb.value = value
        mainb.disabled = false
        mainb.attributes[4].value = fnStr // for because how is declared TODO no magic number
    } else {
        mainb.value = `turno de ${currentPlayer.name}`
    }
}

function submitMeeple() {
    const meeple = get(lastMeeple)
    if (meeple) {
        socket.emit('submit meeple', meeple.parentElement.id)
    }
}

function toggleMeeple(event) {
    const element = event.srcElement
    if (element.id == "lastMeeple") {
        lastMeeple = null;
        socket.emit('remove meeple', element.parentElement.id)
    } else {
        const payload = {
            id: element.id,
            type: element.classList[1]
        }

        socket.emit('put meeple on', payload)
    }
}

function isLastMeeple(element) {
    return element == get(lastMeeple)
}

function drawMeepleOn(meepleDiv, ontoId) {
    if (lastMeeple) {
        get(lastMeeple).innerHTML = ""
    }
    const place = get(ontoId)
    lastMeeple = ontoId
    place.innerHTML = meepleDiv;
}

function submitPlay(tileId) {
    if (!tileId) {
        alert("pon una ficha")
    } else {
        socket.emit('submit play', tileId)
    }
}

function get(id) {
    return document.getElementById(id);
}

function create(type) {
    return document.createElement(type)
}

function createBaseGrid() {
    const grid = create('table')
    grid.id = 'meepleGrid'
    grid.style.display = 'flex'
    return grid;
}

function addMeepleListeners(grid) {
    const sqrs = grid.querySelectorAll('td');
    sqrs.forEach( function(s, index) {
        s.addEventListener('click', toggleMeeple);
    });
}

function removeMeepleListeners() {
    const meepleGrid = get('meepleGrid')
    const sqrs = meepleGrid.querySelectorAll('td');
    sqrs.forEach( function(s, index) {
        s.removeEventListener('click', toggleMeeple);
    });
    cleanIds(meepleGrid)
}

function cleanIds(table) {
    table.children[0].id = null;
    table.querySelectorAll('tr').forEach((e) => {
        e.id = null;
    });
    table.querySelectorAll('td').forEach((e) => {
        e.id = null;
    });
    table.id = null;
}