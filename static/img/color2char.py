"""
    Maps the avg color of square regions of 10px to the closest defined color
    representing it as a defined char
"""
import os
from math import sqrt
from PIL import Image

WHITE = (255, 255, 255)
GREEN = (102, 204, 0)
BROWN = (153, 102, 0)
YELLOW = (204, 204, 0)
RED = (204, 51, 0)
BLUE = (102, 0, 255)
COLORS = [WHITE, GREEN, BROWN, YELLOW, BLUE]
COLOR_CHAR = {WHITE: "w", GREEN: "g", BROWN: "b", YELLOW: "y", RED: "r", BLUE: "a"}


def processAllFiles():
    files = os.popen("ls").read().split("\n")
    output = ""
    for file in files:
        if file[-2:] == "py" or file == "":
            continue
        output += file + "\n\n"
        rep = process_image(file)
        output += str(rep) + "\n"
        output += "------------------------\n\n"

    with open("output", "w") as f:
        f.write(output)

    return 0


def process_image(filename):
    im = Image.open(filename)
    result = []
    for yi in range(10):
        for xi in range(10):
            x = 1 + xi * 10
            y = 1 + yi * 10
            r = im.crop((x, y, x + 10, y + 10))
            r_avg = avg_rgb(r)
            color = closest_color(r_avg)
            char = COLOR_CHAR[color]
            result.append(char)
    return result


def closest_color(color):
    closest = None
    min_delta = 9999999
    for c in COLORS:
        delta = distance(color, c)
        if delta < min_delta:
            closest = c
            min_delta = delta

    return closest


def avg_rgb(roi):
    colors = roi.getcolors()
    colors = [rgba for one, rgba in colors]
    colors = [(r, g, b) for r, g, b, a in colors]
    avg_rgb = []
    for i in range(3):
        avg_rgb.append(list_avg(list(map(lambda rgb: rgb[i], colors))))
    return tuple(avg_rgb)


def list_avg(list):
    return sum(list) / len(list)


def distance(point1, point2):
    x = (point2[0] - point1[0]) ** 2
    y = (point2[1] - point1[1]) ** 2
    z = (point2[2] - point1[2]) ** 2

    return sqrt(x + y + z)

if __name__ == '__main__':
    processAllFiles()
