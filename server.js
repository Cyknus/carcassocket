const express = require('express');
const http = require('http');
const path = require('path');
const socketIO = require('socket.io');
const { State } = require('./logic/state.js')

const app = express();
const server = http.Server(app);
const io = socketIO(server);

app.set('port', 5000);
app.use('/static', express.static(__dirname + '/static'));

app.get('/', function(request, response) {
  response.sendFile(path.join(__dirname, 'index.html'));
});

server.listen(5000, function() {
  console.log("Listening on port 5000")
});

const state = new State();

io.on('connection', function(socket) {
  const sid = socket.id;

  socket.on('new player', function(name) {
    player = { name: name, id: sid, color: 'black'}
    state.players.push(player)
    socket.send(player)
  	io.emit('player in', state);
  });

  socket.on('begin match', function() {
    state.confirmPlayer(sid);
    if (!state.allPlayersConfirmed()) { return; }
    state.pickTile()
    io.emit('playing tile', state)
  })

  socket.on('rotate tile', function(orientation) {
    state.tileOrientation = orientation
    io.emit('rotate to', orientation)
  })

  socket.on('put tile on',  function(tileId) {
    //check if in turn
    io.emit('draw tile', tileId)
  })

  socket.on('submit play', function(tileId) {
    //check if legal
    state.gridHtml = state.getWorkerSlotsHtml();
    io.emit('put a meeple', state)
  })

  socket.on('put meeple on',  function(workerObj) { // NOPE, this is def, We just need to draw the meeple, maybe draw the same over the clicking shit
    const meepleGrid = state.meepleDiv()
    io.emit('draw meeple', meepleGrid, workerObj.id)
  })

  socket.on('remove meeple', function(lastMeeple) {
    io.emit('remove last meeple', lastMeeple);
  })

  socket.on('submit meeple', function(onto) {
    // Check meeple legal
    // minus one meeple for this player
    state.pickTile()
    io.emit('next player', state)
  })

});

