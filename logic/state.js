const palette = require('../assets/colors.js')
exports.State = class {
    #pieces = ['Cccc', 'Ccfc', 'Ccff', 'Ccrc', 'Ccrr', 'Fcfc', 'ccfc',
                    'ccff', 'ccrc', 'ccrr', 'cfcf', 'cfff', 'cfrr', 'cocf',
                    'crfr', 'crrf', 'crrr', 'fcfc', 'ffff', 'ffrf', 'ffrr',
                    'frfr', 'frrr', 'rrrr']

    #frequency = [1, 1, 2, 2, 2, 2, 3,
                       3, 1, 3, 3, 5, 3, 2,
                       4, 3, 3, 1, 4, 2, 9,
                       8, 4, 1]

    #gridGranularity = 10;

    constructor() {
        this.players = [];
        this.turn = 0;
        this.tiles = this.randomTiles();
        this.tileOrientation = 0;
    }

    randomTiles() {
        const tiles = this.createTilesArray()
        this.shuffle(tiles)
        return tiles
    }

    confirmPlayer(playerId) {
        const confirmed = this.confirmed || [];
        if (confirmed.indexOf(playerId) > -1) {
            return;
        } else {
            confirmed.push(playerId)
            this.confirmed = confirmed
        }
    }

    allPlayersConfirmed() {
        return this.confirmed.length == this.players.length
    }

    createTilesArray() {
        let tilesArray = []
        this.#pieces.forEach((p, idx) => {
            const currentPieces = Array(this.#frequency[idx]).fill(p);
            tilesArray = tilesArray.concat(currentPieces)
        })
        return tilesArray
    }

    pickTile() {
        this.currentTile = this.tiles.pop()
    }

    getWorkerSlotsHtml() {
        const rotation = (360 + this.orientation) % 360
        const grid = this.getGrid()
        return grid;
    }

    getGrid() {
        const colors = palette[this.currentTile]
        const granularity = this.#gridGranularity;
        let table = '<tbody id="workerGrid">'
        for (var row = 0; row < granularity; row++) {
            table += `<tr id="r${row}" class="row">`
            for (var column = 0; column < granularity; column++) {
                const cellId = column+row*granularity
                table += `<td id="c${cellId}" `
                table += `class="cell ${colors[cellId]}"></td>`
            }
            table += '</tr>'
        };
        table += '</tbody>'
        return table;
    }

    meeplePlaces(cellId) {
        const granularity = 3;
        const meepleDiv = this.meepleDiv();
        const cell = Number(cellId);
        const nth = mapNinth(cell);
        let table = '<tbody id="meepleGridBody" ';
        table += `style="transform:rotate(${this.tileOrientation}deg)">`;
        for (var row = 0; row < granularity; row++) {
            table += `<tr id="m${row}" class="mrow">`;
            for (var column = 0; column < granularity; column++) {
                const mCellId = column+row*granularity;
                table += `<td id="m${mCellId}" `;
                table += 'class="mcell">';
                if (mCellId == cell) {
                    table += meepleDiv;
                }
                table += '</td>';
            }
            table += '</tr>';
        };
        table += '</tbody>';
        return table;
    }

    meepleDiv() {
        const color = this.players[this.turn].color;
        let div = `<div id="lastMeeple" class="meeple" `;
        div += `style="background-image:url(./static/img/meeple/meeple${color}.png);`;
        div += `transform:rotate(-${this.tileOrientation}deg)">`;
        div += '</div>';
        return div;
    }

    mapNinth(cellId) {
        if (cell%10 < 3) {
            let meeplePlace = 0;
        } else if (cell%10 < 7) {
            let meeplePlace = 1;
        } else {
            let meeplePlace = 2;
        }
        if (cell < 40) {
            meeplePlace += 0;
        } else if (cell > 70) {
            meeplePlace += 3;
        } else {
            meeplePlace += 6;
        }

        return meeplePlace;
    }

    shuffle(array) {
        for (let i = array.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }
}